# Cómo consultar los productos de Precios Justos

Primero se debe contar con la aplicación [Telegram](https://telegram.org)

### 1. En búsqueda global, buscar preciosjustosBot

- Ubique la lupa en la parte superior derecha de la aplicacion telegram, es la busqueda global

![busqueda global](/pjvuepress/precios-justos-bot-0.jpg)

- Escriba el nombre del bot 'preciosjustosbot' y seleccione el bot encontrado que aparecera debajo.

![busqueda global](/pjvuepress/precios-justos-bot-1.jpg)
### 2. Iniciar el bot

![Iniciar el bot](/pjvuepress/precios-justos-bot-2.jpg)

### 3. Haga una primer búsqueda

![Precios Justos busqueda](/pjvuepress/precios-justos-bot-3.jpg)

![Precios Justos busqueda](/pjvuepress/precios-justos-bot-4.jpg)

### 4. Navegue por los resultados con los botones de paginación


![Precios Justos busqueda](/pjvuepress/precios-justos-bot-5.jpg)

### 5. Seleccione el producto

Tocando sobre el codigo de producto, tendra un detalle del mismo.

![Precios Justos busqueda](/pjvuepress/precios-justos-bot-6.jpg)
### 6. Configuración de zonas

En el menu tendra disponible el comando /config para seleccionar la zona o provincia. Precios Justos tiene diferenciados los valores segun la provincia del pais, o region AMBA.

![Precios Justos busqueda](/pjvuepress/precios-justos-bot-7.jpg)

Luego selecciones su zona.

![Precios Justos busqueda](/pjvuepress/precios-justos-bot-8.jpg)

::: warning Busqueda en inline mode
Se puede buscar y compartir resultados en un chat con otra persona o con un grupo. [Ver como](./precios-justos-bot.md).
:::

::: tip Compartí los post de Linkedin de los desarrolladores!
:rocket: [Carlos Lacchini](https://www.linkedin.com/posts/carlos-lacchini_preciosjustos-consumo-inflaciaejn-activity-7000843061387776000-Bzp1?utm_source=share&utm_medium=member_desktop)
:rocket: [Diego Ravecca](https://www.linkedin.com/posts/diego-ravecca_ahora-pod%C3%A9s-buscar-los-productos-de-precios-activity-6999744636827181056-RQ5c?utm_source=share&utm_medium=member_desktop)
:::