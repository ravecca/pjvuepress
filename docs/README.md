---
home: true
heroImage: /preciosjustos.png
actionText: Guía rápida
actionLink: ./precios-justos.md
features:
- title: Simple
  details: Accedé de forma muy simple al valor registrado en el programa Precios Justos.
- title: Rápido
  details: Búsqueda instantánea, navegá fácilmente en los resultados de búsqueda.
- title: Necesario
  details: El acceso a la información es parte de la lucha contra la inflación.
footer: MIT Licensed | Copyright © 2018-present
---

***
 [@preciosjustosBot](https://t.me/preciosjustosBot)

::: warning Esta utilidad no es oficial 
El programa de <strong>Precios Justos</strong> tiene su información oficial en la web del gobierno. [Precios Justos Argentina](https://www.argentina.gob.ar/economia/comercio/preciosjustos)
:::

