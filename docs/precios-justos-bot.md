# Cómo consultar los productos de Precios Justos en inline mode

Inline mode se refiere a llamar a un bot dentro de un chat con otra persona o grupo.

### 1. Mencione al bot con @preciosjustosBot

- En el mismo cuadro de texto donde esta escribiendo los mensajes, se invoca al bot escribiendo @preciosjustosBot, con un espacio y la palabra o frase a buscar.

![busqueda global](/pjvuepress/precios-justos-bot-inline-0.jpg)

- Los productos encontrados con sus precios se mostraran en un listado emergente. Deslice el listado hacia arriba para ver mas resultados. 

![Listado de productos encontrados Precios Justos](/pjvuepress/precios-justos-bot-inline-1.jpg)

### 2. Seleccione el producto

- Puede seleccionar el producto y se enviara un detalle en el mismo chat.

![Seleccione producto de Precios Justos](/pjvuepress/precios-justos-bot-inline-2.jpg)

### 3. Detalle del producto en el chat

- En el chat se enviará un mensaje con el detalle del producto seleccionado. Este mensaje puede reenviarse a otro chat como cualquier otro mensaje de Telegram. En el encabezado puede observar el texto "via @preciosjustosBot", tocando en él vuelve a posicionarse en otra búsqueda inline.

![Detalle producto de Precios Justos](/pjvuepress/precios-justos-bot-inline-3.jpg)